package com.example.musicserver.mapper;

import com.example.musicserver.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-03
 * Time: 8:58
 */
@SpringBootTest
class UserMapperTest {
    @Resource
    private UserMapper userMapper;
    @Test
    void insertUser() {
        User user = new User();
        int ret = userMapper.insertUser("白洋","2001");

        Assertions.assertEquals(ret,1);
    }
}