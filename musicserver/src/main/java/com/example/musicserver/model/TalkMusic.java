package com.example.musicserver.model;

import lombok.Data;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-12
 * Time: 16:40
 */
@Data
public class TalkMusic {
    private int id;
    private int user_id;
    private int music_id;
    private String say;
    private Timestamp saytime;
    private String username;
    private String title;
}
