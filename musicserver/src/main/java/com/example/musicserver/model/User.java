package com.example.musicserver.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-25
 * Time: 16:46
 */
@Data
public class User {
    private int id;
    private String username;
    private String password;
}
