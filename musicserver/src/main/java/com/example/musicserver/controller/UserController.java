package com.example.musicserver.controller;

import com.example.musicserver.mapper.UserMapper;
import com.example.musicserver.model.User;
import com.example.musicserver.service.UserService;
import com.example.musicserver.tools.Constant;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-25
 * Time: 17:04
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserMapper userMapper;

    @Resource
    private UserService userService;

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @RequestMapping("/login")
    public ResponseBodyMessage<User> login(@RequestParam String username, @RequestParam String password,
                                     HttpServletRequest request){
        return userService.login(username,password,request);
    }

    @RequestMapping("/register")
    public ResponseBodyMessage<Boolean> register(@RequestParam String username, @RequestParam String password){
        return userService.register(username,password);
    }

    @RequestMapping("/updatepassword")
    public ResponseBodyMessage<Boolean> update(@RequestParam String username, @RequestParam String password){
        return userService.update(username,password);
    }

    @RequestMapping("/exit")
    public ResponseBodyMessage<Boolean> exit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return userService.exit(request,response);
    }
}
