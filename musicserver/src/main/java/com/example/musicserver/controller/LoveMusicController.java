package com.example.musicserver.controller;

import com.example.musicserver.mapper.LoveMusicMapper;
import com.example.musicserver.model.Music;
import com.example.musicserver.model.User;
import com.example.musicserver.service.LoveMusicService;
import com.example.musicserver.tools.Constant;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-28
 * Time: 15:01
 */
@RestController
@RequestMapping("/lovemusic")
public class LoveMusicController {
    @Resource
    private LoveMusicService loveMusicService;

    @RequestMapping("/likemusic")
    public ResponseBodyMessage<Boolean> insertLoveMusic(@RequestParam String id, HttpServletRequest request){
        return loveMusicService.insertLoveMusic(id,request);
    }

    @RequestMapping("/findlovemusic")
    public ResponseBodyMessage<List<Music>> findLoveMusic(@RequestParam(required = false) String musicName, HttpServletRequest request){
        return loveMusicService.findLoveMusic(musicName,request);
    }

    @RequestMapping("/deletelovemusic")
    public ResponseBodyMessage<Boolean> deletelovemusicById(@RequestParam String id,HttpServletRequest request){
        return loveMusicService.deletelovemusicById(id,request);
    }
}
