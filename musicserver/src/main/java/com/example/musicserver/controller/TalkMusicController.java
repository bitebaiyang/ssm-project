package com.example.musicserver.controller;

import com.example.musicserver.mapper.TalkMusicMapper;
import com.example.musicserver.model.TalkMusic;
import com.example.musicserver.service.TalkService;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-12
 * Time: 16:47
 */
@RestController
@RequestMapping("/talk")
public class TalkMusicController {
    @Resource
    private TalkService talkService;

    @RequestMapping("/talkmusic")
    public ResponseBodyMessage<Boolean> insertTalk(HttpServletRequest request, @RequestParam String musicname, @RequestParam String say){
        return talkService.insertTalk(request,musicname,say);
    }

    @RequestMapping("/findtalk")
    public ResponseBodyMessage<List<TalkMusic>> findTalk(){
        return talkService.findTalk();
    }

    @RequestMapping("/deletetalk")
    public ResponseBodyMessage<Boolean> deleteTalk(@RequestParam String id){
        return talkService.deleteTalk(id);
    }
}
