package com.example.musicserver.controller;

import com.example.musicserver.mapper.LoveMusicMapper;
import com.example.musicserver.mapper.MusicMapper;
import com.example.musicserver.model.Music;
import com.example.musicserver.model.User;
import com.example.musicserver.service.MusicService;
import com.example.musicserver.tools.Constant;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-26
 * Time: 11:22
 */
@RestController
@RequestMapping("/music")
public class MusicController {
    @Resource
    private MusicService musicService;

    @RequestMapping("/upload")
    public ResponseBodyMessage<Boolean> insertMusic(@RequestParam String singer,
                                                    @RequestParam(value = "filename") MultipartFile file,
                                                    HttpServletRequest request,
                                                    HttpServletResponse response) throws IOException {
        return musicService.insertMusic(singer,file,request,response);
    }

    @RequestMapping("/get")
    public ResponseEntity<byte[]> func(String path){
        return musicService.func(path);
    }

    @RequestMapping("/delete")
    public ResponseBodyMessage<Boolean> deleteMusicById(@RequestParam String id){
        return musicService.deleteMusicById(id);
    }

    @RequestMapping("/deleteSel")
    public ResponseBodyMessage<Boolean> deleteSelMusic(@RequestParam("id[]") List<Integer> id){
        return musicService.deleteSelMusic(id);
    }

    @RequestMapping("/findmusic")
    public ResponseBodyMessage<List<Music>> findMusic(@RequestParam(required = false) String name){
        return musicService.findMusic(name);
    }
}
