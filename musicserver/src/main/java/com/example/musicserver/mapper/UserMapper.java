package com.example.musicserver.mapper;

import com.example.musicserver.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-25
 * Time: 16:53
 */
@Mapper
public interface UserMapper {
    User login(User loginUser);
    User selectByName(String username);
    int insertUser(String username, String password);
    int updatePassword(String username, String password);
}
