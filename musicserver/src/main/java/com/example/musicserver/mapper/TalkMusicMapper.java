package com.example.musicserver.mapper;

import com.example.musicserver.model.TalkMusic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-12
 * Time: 16:51
 */
@Mapper
public interface TalkMusicMapper {
    int insertTalk(int user_id, int music_id, String say,String username,String title);
    int deleteTalk(int id);
    List<TalkMusic> findTalk();
}
