package com.example.musicserver.mapper;

import com.example.musicserver.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-26
 * Time: 12:23
 */
@Mapper
public interface MusicMapper {
    //插入音乐
    int insert(String title, String singer, String time, String url, int userId);
    Music select(String title, String singer);
    Music selectById(Integer id);
    int deleteById(Integer id);
    List<Music> findMusic();
    List<Music> findMusicByName(String name);

    int selectIdByName(String title);
}
