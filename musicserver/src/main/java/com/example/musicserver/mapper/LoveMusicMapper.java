package com.example.musicserver.mapper;

import com.example.musicserver.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-28
 * Time: 14:54
 */
@Mapper
public interface LoveMusicMapper {
    Music findLoveMusicByMusicIdAndUserId(int userId, int musicId);
    boolean insertLoveMusic(int userId, int musicId);
    boolean deleteLoveMusic(int userId, int musicId);

    List<Music> findLoveMusicByName(String musicName, int userId);
    List<Music> findLoveMusic(int userId);
    int deleteLoveMusicBymusicId(int musicId);
}
