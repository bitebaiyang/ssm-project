package com.example.musicserver.service;

import com.example.musicserver.mapper.UserMapper;
import com.example.musicserver.model.User;
import com.example.musicserver.tools.Constant;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-10
 * Time: 15:59
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public ResponseBodyMessage<User> login(@RequestParam String username, @RequestParam String password,
                                     HttpServletRequest request){


        User user = userMapper.selectByName(username);

        if(user == null){
            return new ResponseBodyMessage<>(-1,"登录失败",user);
        }else{
            boolean flg = bCryptPasswordEncoder.matches(password,user.getPassword());
            if(!flg){
                return new ResponseBodyMessage<>(-1,"用户名或密码错误",user);
            }
            request.getSession().setAttribute(Constant.USERINFO_SESSION_KEY,user);
            return new ResponseBodyMessage<>(0,"登录成功",user);
        }
    }

    public ResponseBodyMessage<Boolean> register(@RequestParam String username, @RequestParam String password){
        User user = userMapper.selectByName(username);
        if(user == null){

            password = bCryptPasswordEncoder.encode(password);

            int ret = userMapper.insertUser(username,password);
            if(ret == 1){
                return new ResponseBodyMessage<>(0,"注册成功",true);
            }else{
                return new ResponseBodyMessage<>(-1,"注册失败",false);
            }
        }else{
            return new ResponseBodyMessage<>(-1,"该账户已经存在",false);
        }
    }

    public ResponseBodyMessage<Boolean> update(@RequestParam String username, @RequestParam String password){
        User user = userMapper.selectByName(username);

        if(user == null){
            return new ResponseBodyMessage<>(-1,"当前用户不存在无法修改",false);
        }else{
            password = bCryptPasswordEncoder.encode(password);
            int ret = userMapper.updatePassword(username,password);

            if(ret == 1){
                return new ResponseBodyMessage<>(0,"修改成功",true);
            }else{
                return new ResponseBodyMessage<>(-1,"无法修改",false);
            }
        }
    }

    public ResponseBodyMessage<Boolean> exit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);

        session.removeAttribute(Constant.USERINFO_SESSION_KEY);


        return new ResponseBodyMessage<>(0,"退出成功",true);
    }
}
