package com.example.musicserver.service;

import com.example.musicserver.mapper.MusicMapper;
import com.example.musicserver.mapper.TalkMusicMapper;
import com.example.musicserver.model.Music;
import com.example.musicserver.model.TalkMusic;
import com.example.musicserver.model.User;
import com.example.musicserver.tools.Constant;
import com.example.musicserver.tools.ResponseBodyMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-12
 * Time: 16:37
 */
@Service

public class TalkService {
    @Resource
    private MusicMapper musicMapper;
    @Resource
    private TalkMusicMapper talkMusicMapper;

    public ResponseBodyMessage<Boolean> insertTalk(HttpServletRequest request, @RequestParam String musicname, @RequestParam String say){
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(Constant.USERINFO_SESSION_KEY);

        int userId = user.getId();
        String username = user.getUsername();

        int musicId = musicMapper.selectIdByName(musicname);

        int ret = talkMusicMapper.insertTalk(userId,musicId,say,username,musicname);

        if(ret != 1){
            return new ResponseBodyMessage<>(-1,"留言失败",false);
        }else{
            return new ResponseBodyMessage<>(0,"留言成功",true);
        }
    }

    public ResponseBodyMessage<List<TalkMusic>> findTalk(){
        List<TalkMusic> list = talkMusicMapper.findTalk();

        return new ResponseBodyMessage<>(0,"查询成功",list);
    }

    public ResponseBodyMessage<Boolean> deleteTalk(String id){
        int id1 = Integer.parseInt(id);
        int ret = talkMusicMapper.deleteTalk(id1);

        if(ret == 1){
            return new ResponseBodyMessage<>(0,"删除成功",true);
        }else{
            return new ResponseBodyMessage<>(-1,"删除失败",false);
        }
    }
}
