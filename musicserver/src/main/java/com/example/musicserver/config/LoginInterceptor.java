package com.example.musicserver.config;

import com.example.musicserver.tools.Constant;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-02
 * Time: 17:21
 */

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);

        if(session != null && session.getAttribute(Constant.USERINFO_SESSION_KEY) != null){
            System.out.println("登录成功");
            return true;
        }

        response.sendRedirect("/login.html");
        return false;
    }
}
