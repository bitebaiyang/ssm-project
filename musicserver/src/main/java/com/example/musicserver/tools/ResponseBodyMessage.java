package com.example.musicserver.tools;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-07-26
 * Time: 9:23
 */
@Data
public class ResponseBodyMessage<T>{
    private int status;//状态码
    private String message;//返回的信息【出错原因】
    private T data;//返回个前端的数据

    public ResponseBodyMessage(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
