drop database if exists onlinemusic;

create database if not exists onlinemusic character set utf8;
use onlinemusic;

drop table if exists user;
create table user(id int primary key auto_increment, username varchar(20) not null, `password` varchar(255) not null);

drop table if exists music;
create table music(id int primary key auto_increment, title varchar(50) not null, singer varchar(30) not null, `time` varchar(13) not null, `url` varchar(1000) not null, userId int(11) not null);

drop table if exists lovemusic;
create table lovemusic(id int primary key auto_increment, user_id int(11) not null, music_id int(11) not null);

drop table if exists talkmusic;
create table talkmusic(id int primary key auto_increment, user_id int(11) not null, music_id int(11) not null, say mediumtext not null, saytime datetime, username varchar(50), title varchar(50));