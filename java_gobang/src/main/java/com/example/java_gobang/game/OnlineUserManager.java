package com.example.java_gobang.game;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-16
 * Time: 9:35
 */
//使用这个类来维护用户在线状态
@Component
public class OnlineUserManager {
    //当前用户在游戏大厅的状态
    private ConcurrentHashMap<Integer, WebSocketSession> gameHall = new ConcurrentHashMap<>();
    //当前用户在游戏房间的在线状态
    private ConcurrentHashMap<Integer,WebSocketSession> gameRoom = new ConcurrentHashMap<>();

    public void enterGameHall(int userId, WebSocketSession session){
        gameHall.put(userId,session);
    }

    public void exitGameHall(int userId){
        gameHall.remove(userId);
    }

    public WebSocketSession getFromGameHall(int userId){
        return gameHall.get(userId);
    }
    public void enterGameRoom(int userId, WebSocketSession session){
        gameRoom.put(userId,session);
    }

    public void exitGameRoom(int userId){
        gameRoom.remove(userId);
    }

    public WebSocketSession getFromGameRoom(int userId){
        return gameRoom.get(userId);
    }
}
