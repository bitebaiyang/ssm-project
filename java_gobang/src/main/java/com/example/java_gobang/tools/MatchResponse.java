package com.example.java_gobang.tools;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-16
 * Time: 9:52
 */
@Data
public class MatchResponse {
    private boolean ok;
    private String reason;
    private String message;
}
