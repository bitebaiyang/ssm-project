package com.example.java_gobang.tools;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-17
 * Time: 16:07
 */
//落子请求
@Data
public class GameRequest {
    private String message;
    private int userId;
    private int row;
    private int col;
}
