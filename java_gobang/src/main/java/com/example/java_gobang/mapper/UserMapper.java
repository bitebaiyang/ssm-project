package com.example.java_gobang.mapper;

import com.example.java_gobang.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-15
 * Time: 10:58
 */
@Mapper
public interface UserMapper {
    //注册
    int insert(User user);

    //登录
    User selectByName(String username);

    //总场次 + 1 获胜 + 1 天梯分数 + 30
    void userWin(int userId);

    //总场次 + 1 获胜不变 天梯分数 - 30
    void userLose(int userId);
}
