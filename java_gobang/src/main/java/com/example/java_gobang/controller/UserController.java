package com.example.java_gobang.controller;

import com.example.java_gobang.model.User;
import com.example.java_gobang.service.UserService;
import com.example.java_gobang.tools.ResponseBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Lenovo
 * Date: 2022-08-15
 * Time: 11:24
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public ResponseBodyMessage<User> userLogin(HttpServletRequest request, @RequestParam String username, @RequestParam String password){
        return userService.userLogin(request,username,password);
    }

    @RequestMapping("/register")
    public ResponseBodyMessage<User> userRegister(@RequestParam String username, @RequestParam String password){
        return userService.userRegister(username,password);
    }

    @RequestMapping("/userinfo")
    public ResponseBodyMessage<User> getUserInfo(HttpServletRequest request){
        return userService.getUserInfo(request);
    }
}
